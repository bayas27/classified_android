package com.vinbytes.olonce.ui

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.ProgressBar

/**
 * Created by Sabish.M on 06/09/18.
 * Copyright (c) 2017 T-Tech. All rights reserved.
 */
class PleaseWait(context: Context) : Dialog(context, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen) {
    init {
        val root = LinearLayout(context)
        root.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        val color = Color.argb(150, 0, 0, 0)
        root.setBackgroundColor(color)

        setContentView(root)
        setCancelable(false)
        setCanceledOnTouchOutside(false)

        root.gravity = Gravity.CENTER
        val progressBar = ProgressBar(context)
        if (progressBar.progressDrawable != null) {
            progressBar.progressDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)
        }
        if (progressBar.indeterminateDrawable != null) {
            progressBar.indeterminateDrawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN)
        }
        progressBar.isIndeterminate = true
        root.addView(progressBar)
    }

}