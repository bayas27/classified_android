package com.vinbytes.olonce;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.vinbytes.olonce.helpers.Connections;
import com.vinbytes.olonce.helpers.MD5Kt;
import com.vinbytes.olonce.helpers.Response;
import com.vinbytes.olonce.ui.PleaseWait;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;


public class LoginFragment extends Fragment implements View.OnClickListener {

    public LoginFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        initSignUpBtn();
        if (getView() != null) {
            getView().findViewById(R.id.login_btn).setOnClickListener(this);
            getView().findViewById(R.id.login_facebook_btn).setOnClickListener(this);
            getView().findViewById(R.id.login_google_btn).setOnClickListener(this);
            getView().findViewById(R.id.login_forgot_password).setOnClickListener(this);
        }
    }

    private void initSignUpBtn() {
            SpannableString ss = new SpannableString("Don't have an account? SIGN UP");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(@NotNull View textView) {

            }

            @Override
            public void updateDrawState(@NotNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
            }
        };

        ss.setSpan(clickableSpan, "Don't have an account? ".length() - 1, "Don't have an account? SIGN UP".length() - 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        if (getView() != null) {
            TextView terms = getView().findViewById(R.id.login_signup_btn);
            terms.setText(ss);
            terms.setMovementMethod(LinkMovementMethod.getInstance());
            terms.setHighlightColor(Color.BLUE);
        }
    }

    private void login() {
        if (getView() != null) {
            String username = ((EditText) getView().findViewById(R.id.login_email)).getText().toString();
            String password = ((EditText) getView().findViewById(R.id.login_password)).getText().toString();

            if (username.length() > 0 && password.length() > 0) {
                PleaseWait pleaseWait = null;
                if (getContext() != null) {
                    pleaseWait = new PleaseWait(getContext());
                    pleaseWait.show();
                }
                try {
                    final PleaseWait finalPleaseWait = pleaseWait;
                    Connections.login(new JSONObject().put("emailId", username).put("password", MD5Kt.md5(password)),
                            new Connections.CallBack() {
                                @Override
                                public void response(final Response response) {
                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (finalPleaseWait != null && finalPleaseWait.isShowing())
                                                finalPleaseWait.dismiss();
                                            if (response.getResponseCode() == HttpURLConnection.HTTP_OK && !response.getResponse().isEmpty()) {
                                                try {
                                                    JSONObject jsonObject = new JSONObject(response.getResponse());
                                                    if (jsonObject.has("status") && jsonObject.getString("status").toLowerCase().equals("success")) {
                                                        if (response.headerFields.containsKey("Authorization") && response.headerFields.get("Authorization") != null &&
                                                                response.headerFields.get("Authorization").size() > 0) {
                                                            if (getActivity() != null) {
                                                                SharedPreferences sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
                                                                SharedPreferences.Editor editor = sharedPreferences.edit();
                                                                editor.putString("Authorization", response.headerFields.get("Authorization").get(0));
                                                                editor.apply();
                                                            }
                                                        }
                                                    } else {
                                                        System.out.println("Login Failed");
                                                    }
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                    });
                                }
                            });
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            } else if (username.length() == 0) {
                ((EditText) getView().findViewById(R.id.login_email)).setError("Please enter valid Email Address / Phone No.");
            } else {
                ((EditText) getView().findViewById(R.id.login_password)).setError("Please enter valid password.");
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login_btn) {
            this.login();
        } else if (v.getId() == R.id.login_google_btn) {

        } else if (v.getId() == R.id.login_facebook_btn) {

        } else if (v.getId() == R.id.login_forgot_password) {

        }
    }
}
