package com.vinbytes.olonce.helpers;

/**
 * Created by Sabish on 6/17/2017.
 * Copyright (c) 2017 T-Tech. All rights reserved.
 */

public class Interface {
    public interface DoInBackground {
        void doInBackground();

        void finish();
    }

    public interface Response {
        void callBack(int responseCode, Object response);
    }
}