package com.vinbytes.olonce.helpers

import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import javax.net.ssl.HttpsURLConnection

/**
 * Created by Sabish on 3/15/2017.
 * Copyright (c) 2017 T-Tech. All rights reserved.
 */

class Post(link: String) {

    private var data = ""
    private var url_data = ""
    lateinit var conn: HttpURLConnection

    val jsonObject: JSONObject
        get() = JSONObject()

    init {
        var url: URL?
        try {
            url = URL(link)
            conn = url.openConnection() as HttpURLConnection
            conn.doOutput = true
            conn.readTimeout = 15000
            conn.connectTimeout = 15000
            conn.requestMethod = "POST"
            conn.doInput = true
            conn.doOutput = true
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    fun setPatchMethod() {
        conn.requestMethod = "PATCH"
        conn.setRequestProperty("X-HTTP-Method-Override", "PATCH")
    }

    fun addFormField(key: String, value: String?) {
        var value1 = value
        try {
            if (!data.isEmpty())
                data += "&"
            if (value1 == null || value1.isEmpty())
                value1 = ""
            data += URLEncoder.encode(key, "UTF-8") + "=" + URLEncoder.encode(value1, "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }

    }

    fun addFormField(key: String, value: Int) {
        try {
            if (!data.isEmpty())
                data += "&"
            data += URLEncoder.encode(key, "UTF-8") + "=" + URLEncoder.encode(Integer.toString(value), "UTF-8")
        } catch (e: UnsupportedEncodingException) {
            e.printStackTrace()
        }
    }


    fun finish(): Response? {
        try {
            val os = conn.outputStream
            val writer = BufferedWriter(
                    OutputStreamWriter(os, "UTF-8"))

            writer.write(data)

            writer.flush()
            writer.close()
            os.close()

            val responseCode = conn.responseCode


            val stringBuilder = StringBuilder()
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                val input = BufferedReader(InputStreamReader(conn.inputStream), 8192)
                var strLine: String? = null
                while ({ strLine = input.readLine(); strLine }() != null) {
                    stringBuilder.append(strLine)
                }
                input.close()
                return Response(conn.responseCode, stringBuilder.toString())
            } else
                return null
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }

    }

    @Throws(IOException::class)
    fun finish(jsonObject: JSONObject): Response {
        conn.setRequestProperty("Content-Type", "application/json")
        conn.setRequestProperty("Accept", "application/json")
        //Write
        var os: OutputStream? = null
        try {
            os = conn.outputStream
        } catch (e: Exception) {
            throw IOException("Connection Error")
        }
        val writer = BufferedWriter(OutputStreamWriter(os, "UTF-8"))
        writer.write(jsonObject.toString())
        writer.close()
        os!!.close()

        return if (conn.responseCode == HttpURLConnection.HTTP_OK || conn.responseCode == HttpURLConnection.HTTP_CREATED || conn.responseCode == HttpURLConnection.HTTP_ACCEPTED) {
            val br = BufferedReader(InputStreamReader(conn.inputStream, "UTF-8"))

            var line: String? = null
            val sb = StringBuilder()

            while ({ line = br.readLine(); line }() != null) {
                sb.append(line)
            }

            br.close()
            conn.inputStream.close()
            try {
                Response(200, sb.toString(), conn.headerFields)
            } catch (e: JSONException) {
                e.printStackTrace()
                throw IOException("")
            }
        } else try {
            Response(200, "")
        } catch (e: JSONException) {
            throw IOException("")
        }
    }
}