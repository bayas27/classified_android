package com.vinbytes.olonce.helpers;

import org.json.JSONObject;

import java.io.IOException;

public class Connections {
    public static void login(final JSONObject reqData, final Connections.CallBack callBack) {
        new Fetch(new Interface.DoInBackground() {
            @Override
            public void doInBackground() {
                Post post = new Post(Links.LOGIN);
                Response response = null;
                try {
                    response = post.finish(reqData);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (callBack != null) {
                    callBack.response(response);
                }
            }

            @Override
            public void finish() {

            }
        }).execute();
    }
    public static void register(final JSONObject reqData, final Connections.CallBack callBack) {
        new Fetch(new Interface.DoInBackground() {
            @Override
            public void doInBackground() {
                Post post = new Post(Links.LOGIN);
                Response response = null;
                try {
                    response = post.finish(reqData);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (callBack != null) {
                    callBack.response(response);
                }
            }

            @Override
            public void finish() {

            }
        }).execute();
    }

    public static void forgotPassword() {

    }

    public interface CallBack {
        void response(Response response);
    }
}
