package com.vinbytes.olonce.helpers

import android.os.AsyncTask

/**
 * Created by Sabish on 3/13/2017.
 * Copyright (c) 2017 T-Tech. All rights reserved.
 */

class Fetch(private val doInBackground: Interface.DoInBackground?) : AsyncTask<Int, Void, String>() {
    override fun doInBackground(vararg params: Int?): String {
        doInBackground?.doInBackground()
        return ""
    }

    override fun onPostExecute(s: String) {
        super.onPostExecute(s)
        doInBackground!!.finish()
    }

    fun cancel() {
        this.cancel(true)
    }
}