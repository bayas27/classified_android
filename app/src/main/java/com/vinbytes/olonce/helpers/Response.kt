package com.vinbytes.olonce.helpers

class Response(responseCode: Int, response: String) {
    var responseCode: Int = 0
    var response: String
    lateinit var headerFields: Map<String, List<String>>

    init {
        this.responseCode = responseCode
        this.response = response
    }

    constructor(responseCode: Int, response: String, headerFields: Map<String, List<String>>) : this(responseCode, response) {
        this.headerFields = headerFields
    }
}