package com.vinbytes.olonce;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.vinbytes.olonce.helpers.Connections;
import com.vinbytes.olonce.helpers.MD5Kt;
import com.vinbytes.olonce.helpers.Response;
import com.vinbytes.olonce.ui.PleaseWait;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.HttpURLConnection;

public class RegisterFragment extends Fragment implements View.OnClickListener  {
    
    public RegisterFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews();
    }

    private void initViews() {
        initSignInBtn();
        if (getView() != null) {
            getView().findViewById(R.id.signup_btn).setOnClickListener(this);
            getView().findViewById(R.id.signup_facebook_btn).setOnClickListener(this);
            getView().findViewById(R.id.signup_google_btn).setOnClickListener(this);
        }
    }

    private void initSignInBtn() {
        SpannableString lg = new SpannableString("Have an account? SIGN IN");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(@NotNull View textView) {

            }
        };
        lg.setSpan(clickableSpan,"Have an account?".length()-1,"Have an account ? SIGN IN".length()-1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        if(getView()!=null){
            TextView terms = getView().findViewById(R.id.signup_Have_an_acc);
            terms.setText(lg);
            terms.setMovementMethod(LinkMovementMethod.getInstance());
            terms.setHighlightColor(Color.BLUE);
        }

    }

    private void signup(){
        if(getView()!=null){
            String email = ((EditText) getView().findViewById(R.id.signup_email)).getText().toString();
            String phoneNo = ((EditText) getView().findViewById(R.id.signup_Phone)).getText().toString();
            String password = ((EditText) getView().findViewById(R.id.signup_Password)).getText().toString();
            String confirmPassword = ((EditText) getView().findViewById(R.id.signup_Confirmpassword)).getText().toString();
            if(email.length()>0 && phoneNo.length()>0 && password.length()>0 && confirmPassword.length()>0){
                if(password==confirmPassword){
                    PleaseWait pleaseWait = null;
                    if (getContext() != null) {
                        pleaseWait = new PleaseWait(getContext());
                        pleaseWait.show();
                    }
                    try {
                        final PleaseWait finalPleaseWait = pleaseWait;
                        Connections.register(new JSONObject().put("emailId", email).put("password", MD5Kt.md5(password)).put("phoneNumber", phoneNo),
                                new Connections.CallBack() {
                                    @Override
                                    public void response(final Response response) {
                                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (finalPleaseWait != null && finalPleaseWait.isShowing())
                                                    finalPleaseWait.dismiss();
                                                if (response.getResponseCode() == HttpURLConnection.HTTP_OK && !response.getResponse().isEmpty()) {
                                                    try {
                                                        JSONObject jsonObject = new JSONObject(response.getResponse());
                                                        if (jsonObject.has("status") && jsonObject.getString("status").toLowerCase().equals("success")) {
                                                            System.out.println("SIgned Up Successfully");
                                                        } else {
                                                            System.out.println(jsonObject.getString("responseMessage"));
                                                        }
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        });
                                    }
                                });
                    } catch (JSONException ex) {
                        ex.printStackTrace();
                    }
                }
                else{
                    ((EditText) getView().findViewById(R.id.signup_Confirmpassword)).setError("Confirm password not matched");
                }
            }
            else if(email.length()==0){
                ((EditText) getView().findViewById(R.id.signup_email)).setError("Please enter valid the Email Address");
            }
            else if(phoneNo.length()==0){
                ((EditText) getView().findViewById(R.id.signup_Phone)).setError("Please enter valid the Phone Number");
            }
            else if(password.length()==0){
                ((EditText) getView().findViewById(R.id.signup_Password)).setError("Please enter valid the Password");
            }
            else if(password.length()==0){
                ((EditText) getView().findViewById(R.id.signup_Confirmpassword)).setError("Please enter valid the Confirm Password");
            }
        }
    }
    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.signup_btn) {
            this.signup();
        } else if (v.getId() == R.id.signup_google_btn) {

        } else if (v.getId() == R.id.signup_facebook_btn) {

        }
    }
}
